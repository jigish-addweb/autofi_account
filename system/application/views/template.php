<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="UTF-8">
<title>Auto Fi<?php if (isset($page_title)) echo ' | ' . $page_title; ?></title>

<?php echo link_tag(asset_url() . 'images/favicon.ico', 'shortcut icon', 'image/ico'); ?>

<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/style.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/tables.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/custom.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/menu.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/jquery.datepick.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/thickbox.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>fa/css/font-awesome.css">
<link type="text/css" rel="stylesheet" href="<?php echo asset_url(); ?>css/default.css">

<?php
/* Dynamically adding css files from controllers */
if (isset($add_css))
{
	foreach ($add_css as $id => $row)
	{
		echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . asset_url() . $row ."\">";
	}
}
?>

<script type="text/javascript">
	var jsSiteUrl = '<?php echo base_url(); ?>';
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo asset_url(); ?>js/jquery.min.js"></script><?php */?>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/app.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/custom.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/hoverIntent.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/superfish.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/supersubs.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/thickbox-compressed.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/ezpz_tooltip.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/shortcutslibrary.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/shortcuts.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo asset_url(); ?>js/responsive-nav.js"></script><?php */?>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/jquery.slimscroll.min.js"></script>


<?php
/* Dynamically adding javascript files from controllers */
if (isset($add_javascript))
{
	foreach ($add_javascript as $id => $row)
	{
		echo "<script type=\"text/javascript\" src=\"" . asset_url() . $row ."\"></script>";
	}
}
?>

<script type="text/javascript">
var $ = jQuery.noConflict();
/* Loading JQuery Superfish menu */
$(document).ready(function() {
	/*$("ul.sf-menu").supersubs({ 
		minWidth:12,
		maxWidth:27,
		extraWidth: 1
	}).superfish();*/ // call supersubs first, then superfish, so that subs are 
	$('.datepicker').datepick({
		dateFormat: '<?php echo $this->config->item('account_date_format'); ?>',
	});
	$('.datepicker-restrict').datepick({
		dateFormat: '<?php echo $this->config->item('account_date_format'); ?>',
		minDate: '<?php echo date_mysql_to_php($this->config->item('account_fy_start')); ?>',
		maxDate: '<?php echo date_mysql_to_php($this->config->item('account_fy_end')); ?>',
	});
});
</script>

</head>
<body>
<div id="container">
	<div class="main-header">
		<div id="logo">
			<?php echo anchor('', 'Auto Fi', array('class' => 'anchor-link-b')); ?>
		</div>
	
		<div id="menu" class="">
			<nav class="mainmaenu nav-collapse">
				<ul class="sf-menu main">
					<?php /* 
					?><li class="current">
						<a href="<?php print base_url(); ?>" title="Dashboard">Dashboard</a>
					</li>
					<li>
						<?php echo anchor('account', 'Accounts', array('title' => 'Chart of accounts')); ?>
					</li>
					<li>
						<?php
							$entry_type_all = $this->config->item('account_entry_types');
							$entry_type_count = count($entry_type_all);
							if ($entry_type_count < 1)
							{
								echo "";
							} else if ($entry_type_count == 1) {
								foreach ($entry_type_all as $id => $row)
								{
									echo anchor('entry/show/' . $row['label'], $row['name'], array('title' => $row['name'] . ' Entries'));
								}
							} else {
								echo anchor('entry', "Entries", array('title' => ENTRIES));
								echo "<ul>";
								echo "<li>" . anchor('entry/show/all', 'All', array('title' => 'All Entries')) . "</li>";
								foreach ($entry_type_all as $id => $row)
								{
									echo "<li>" . anchor('entry/show/' . $row['label'], $row['name'], array('title' => $row['name'] . ' Entries')) . "</li>";
								}
								echo "</ul>";
							}
						?>
					</li>
					<li>
						<?php echo anchor('report', 'Reports', array('title' => 'Reports')); ?>
						<ul>
							<li><?php echo anchor('report/balancesheet', 'Balance Sheet', array('title' => 'Balance Sheet')); ?></li>
							<li><?php echo anchor('report/profitandloss', 'Profit & Loss', array('title' => 'Profit & Loss')); ?></li>
							<li><?php echo anchor('report/trialbalance', 'Trial Balance', array('title' => 'Trial Balance')); ?></li>
							<li><?php echo anchor('report/ledgerst', 'Ledger Statement', array('title' => 'Ledger Statement')); ?></li>
							<li><?php echo anchor('report/reconciliation/pending', 'Reconciliation', array('title' => 'Reconciliation')); ?></li>
						</ul>
					</li>
					<li>
						<?php echo anchor('setting', "Settings", array('title' => SETTINGS)); ?>
					</li>
					<li>
						<?php echo anchor('help', 'Help', array('title' => 'Help', 'class' => 'last')); ?>
					</li><?php */?>
					<li class="user">
						<a href="#"><i class="fa fa-user"></i> User</a>
						<?php
							if ($this->session->userdata('user_name')) {
								echo "<ul>";
								echo "<div id=\"admin\">";
								echo "<li>";
								echo anchor('', '<i class="fa fa-file-text-o"></i> Accounts', array('title' => "Accounts", 'class' => 'anchor-link-b'));
								echo "</li>";
								/* Check if allowed administer rights */
								if (check_access('administer')) {
									echo "<li>";
									echo anchor('admin', '<i class="fa fa-tasks"></i> Administer', array('title' => "Administer", 'class' => 'anchor-link-b'));
									echo "</li>";
								}
								echo "<li>";
								echo anchor('user/profile', '<i class="fa fa-user"></i> Profile', array('title' => "Profile", 'class' => 'anchor-link-b'));
								echo "</li>";
								echo "<li>";
								echo anchor('user/logout', '<i class="fa fa-power-off"></i> Logout', array('title' => "Logout", 'class' => 'anchor-link-b'));
								echo "</li>";
								echo "</div>";
								echo "</ul>";
							}
						?>
					</li>
					<li class="mobile-menu-btn">
						<a href="#">
							<img src="<?php echo asset_url(); ?>images/menu-toggler.png" alt="Menu" />
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div><!--main-header-->
	
	<div class="page-container">
	
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->        
		<ul class="page-sidebar-menu">
			<li class="desktop-show">
			   <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			   <div class="sidebar-toggler hidden-phone"></div>
			   <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>
			<li class="start active ">
				<a href="<?php print base_url(); ?>" title="Dashboard">
					<i class="char char-D"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="">
				<?php echo anchor('account', '<i class="char char-A"></i> <span class="title">Accounts</span>', array('title' => 'Chart of accounts')); ?>
			</li>
			<li class="">
				<?php
					/* Showing Entry Type sub-menu */
					$entry_type_all = $this->config->item('account_entry_types');
					$entry_type_count = count($entry_type_all);
					if ($entry_type_count < 1)
					{
						echo "";
					}
					else if ($entry_type_count == 1) {
						foreach ($entry_type_all as $id => $row)
						{
							echo anchor('entry/show/' . $row['label'], $row['name'], array('title' => $row['name'] . ' Entries'));
						}
					}
					else {
						echo anchor('entry', '<i class="char char-E"></i> <span class="title">Entries</span><span class="arrow "></span>', array('title' => ENTRIES));
						
						echo '<ul class="sub-menu">';
						echo "<li>" . anchor('entry/show/all', '<i class="char char-A"></i> All', array('title' => 'All Entries')) . "</li>";
						foreach ($entry_type_all as $id => $row)
						{
							echo "<li>" . anchor('entry/show/' . $row['label'], '<i class="char char-' . substr($row['name'], 0, 1) . '"></i> ' . $row['name'] , array('title' => $row['name'] . ' Entries')) . "</li>";
						}
						echo "</ul>";
					}
			
			   ?>
			   
			</li>
			<li class="">
				<?php echo anchor('report', '<i class="char char-R"></i> <span class="title">Reports</span><span class="arrow "></span>', array('title' => 'Reports')); ?>
				<ul class="sub-menu">
					<li><?php echo anchor('report/balancesheet', '<i class="char char-B"></i> Balance Sheet', array('title' => 'Balance Sheet')); ?></li>
					<li><?php echo anchor('report/profitandloss', '<i class="char char-P"></i> Profit & Loss', array('title' => 'Profit & Loss')); ?></li>
					<li><?php echo anchor('report/trialbalance', '<i class="char char-T"></i> Trial Balance', array('title' => 'Trial Balance')); ?></li>
					<li><?php echo anchor('report/ledgerst', '<i class="char char-L"></i> Ledger Statement', array('title' => 'Ledger Statement')); ?></li>
					<li><?php echo anchor('report/reconciliation/pending', '<i class="char char-R"></i> Reconciliation', array('title' => 'Reconciliation')); ?></li>
				</ul>
			</li>
			<li class="">
				<?php echo anchor('setting', "<i class='char char-S'></i> <span class='title'>Settings</span>", array('title' => SETTINGS)); ?>
			</li>
			<li class="">
				<?php echo anchor('help', '<i class="char char-H"></i> <span class="title">Help</span>', array('title' => 'Help', 'class' => 'last')); ?>
			</li>
			<li class="last">
				<a href="#"><i class="char fa fa-user"></i> <span class="arrow ">User</span></a>
				<?php
					if ($this->session->userdata('user_name')) {
						echo "<ul class='sub-menu'>";
						//echo "<div id=\"admin\">";
						echo "<li>";
						echo anchor('', '<i class="char char-A"></i> Accounts', array('title' => "Accounts", 'class' => 'anchor-link-b'));
						echo "</li>";
						/* Check if allowed administer rights */
						if (check_access('administer')) {
							echo "<li>";
							echo anchor('admin', '<i class="char char-A"></i> Administer', array('title' => "Administer", 'class' => 'anchor-link-b'));
							echo "</li>";
						}
						echo "<li>";
						echo anchor('user/profile', '<i class="char char-P"></i> Profile', array('title' => "Profile", 'class' => 'anchor-link-b'));
						echo "</li>";
						echo "<li>";
						echo anchor('user/logout', '<i class="char fa fa-power-off"></i> Logout', array('title' => "Logout", 'class' => 'anchor-link-b'));
						echo "</li>";
						//echo "</div>";
						echo "</ul>";
					}
				?>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
	
	<div id="content" class="page-content">
		<div id="header" class="tb-row">
			<div class="top-box-4 tb1">
				<?php echo anchor('entry/add/receipt', '<i class="fa fa-list-alt"></i> Receipt', array('title' => 'Receipt', 'class' => 'h-btn')); ?>				
			</div>
			<div class="top-box-4 tb2">
				<?php echo anchor('entry/add/payment', '<i class="fa fa-inr"></i> Payment', array('title' => 'Payment', 'class' => 'h-btn')); ?>				
			</div>
			<div class="top-box-4 tb3">
				<?php echo anchor('entry/add/contra', '<i class="fa fa-file"></i> Contra', array('title' => 'Contra', 'class' => 'h-btn')); ?>
			</div>
			<div class="top-box-4 tb4">
				<?php echo anchor('entry/add/journal', '<i class="fa fa-book"></i> Journal', array('title' => 'Journal', 'class' => 'h-btn')); ?>			
			</div>
			<div class="top-box-4 tb5" id="info">
				<?php
					echo $this->config->item('account_name');
					echo " (";
					echo anchor('user/account', 'change', array('title' => 'Change active account', 'class' => 'anchor-link-a'));
					echo ")<br />";
					echo "FY : ";
					echo date_mysql_to_php_display($this->config->item('account_fy_start'));
					echo " - ";
					echo date_mysql_to_php_display($this->config->item('account_fy_end'));
				?>
			</div>
		</div>
	
		<div id="sidebar">
			<?php if (isset($page_sidebar)) echo $page_sidebar; ?>
		</div>
		
		<div id="main">
			<div id="main-title">
				<?php if (isset($page_title)) echo $page_title; ?>
			</div>
			<?php if (isset($nav_links)) {
				echo "<div id=\"main-links\">";
				echo "<ul id=\"main-links-nav\">";
				foreach ($nav_links as $link => $title) {
					if ($title == "Print Preview")
						echo "<li>" . anchor_popup($link, $title, array('title' => $title, 'class' => 'nav-links-item', 'style' => 'background-image:url(\'' . asset_url() . 'images/buttons/navlink.png\');', 'width' => '1024')) . "</li>";
					else
						echo "<li>" . anchor($link, $title, array('title' => $title, 'class' => 'nav-links-item', 'style' => 'background-image:url(\'' . asset_url() . 'images/buttons/navlink.png\');')) . "</li>";
				}
				echo "</ul>";
				echo "</div>";
			} ?>
			<div class="clear">
			</div>
			<div id="main-content">
				<?php
				$messages = $this->messages->get();
				if (is_array($messages))
				{
					if (count($messages['success']) > 0)
					{
						echo "<div id=\"success-box\">";
						echo "<ul>";
						foreach ($messages['success'] as $message) {
							echo ('<li>' . $message . '</li>');
						}
						echo "</ul>";
						echo "</div>";
					}
					if (count($messages['error']) > 0)
					{
						echo "<div id=\"error-box\">";
						echo "<ul>";
						foreach ($messages['error'] as $message) {
							if (substr($message, 0, 4) == "<li>")
								echo ($message);
							else
								echo ('<li>' . $message . '</li>');
						}
						echo "</ul>";
						echo "</div>";
					}
					if (count($messages['message']) > 0)
					{
						echo "<div id=\"message-box\">";
						echo "<ul>";
						foreach ($messages['message'] as $message) {
							echo ('<li>' . $message . '</li>');
						}
						echo "</ul>";
						echo "</div>";
					}
				}
				?>
				<?php echo $contents; ?>
			</div>
		</div>
	</div>
	
	</div><!--page-container-->
</div>
<div id="footer">
	<?php if (isset($page_footer)) echo $page_footer ?>
	<a href="http://autofi.in" target="_blank">Auto Fi<a/> is licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License, Version 2.0</a>
</div>

	<script>
		/*var navigation1 = responsiveNav(".mainmaenu");
		var navigation2 = responsiveNav(".nav-2", {
			insert: "before"
		});*/
	</script>
	<script>
      $(document).ready(function() {    
         App.init(); // initlayout and core plugins
      });
    </script>
		
</body>
</html>
