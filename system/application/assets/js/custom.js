/* Confirm click */

var custQuery = jQuery.noConflict();

custQuery(document).ready(function() {
	custQuery(".confirmClick").click( function() { 
	    if (custQuery(this).attr('title')) {
		var question = 'Are you sure you want to ' + custQuery(this).attr('title').toLowerCase() + '?';
	    } else {
		var question = 'Are you sure you want to do this action?';
	    }
	    if ( confirm( question ) ) {
		[removed].href = this.src;
	    } else {
		return false;
	    }
	});
	custQuery("#tooltip-target-1").ezpz_tooltip();
	custQuery("#tooltip-target-2").ezpz_tooltip();
	custQuery("#tooltip-target-3").ezpz_tooltip();
	
	custQuery(".mobile-menu-btn a").click( function() { 
		custQuery(".page-sidebar").toggle();
	});
})


